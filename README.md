# Trip Planner UI

This project was built using node 7.9.0 and dependencies were managed with [yarn](https://yarnpkg.com/en/).

## Installation
```
yarn install
```
or
```
npm install
```

## Running 

**You need to start the fake server** in a separate terminal session before starting the application:

```
npm run start:server
```

Then to start the main application:
```
npm start
```