import React from 'react';
import CreateTripForm from './component.js';
import createTrip from './api';
import validate from 'validate.js';
import constraints from './validation';
import { withRouter } from 'react-router-dom';

const defaultState = {
  name: '',
  destination: '',
  dates_start: '',
  dates_end: '',
  errors: undefined,
};

class Container extends React.Component {
  constructor() {
    super();

    this.handleFormChange = this.handleFormChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);

    this.state = { ...defaultState };
  }

  handleFormChange(field, value) {
    this.setState({ [field]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    const errors = validate(this.state, constraints);

    if (typeof errors !== 'undefined') {
      return this.setState(state => ({
        ...this.state,
        errors,
      }));
    }

    const { name, destination, dates_start, dates_end } = this.state;

    const { history } = this.props;
    createTrip({ name, destination, dates_start, dates_end })
      .then(() => history.push('/'))
      // TODO: Add a reqest error state for the form
      .catch(error => console.log(error));  
  }

  handleCancel(e) {
    e.preventDefault();
    const { history } = this.props;
    history.goBack();
  }

  render() {
    return (
      <CreateTripForm
        values={this.state}
        onSubmit={this.handleSubmit}
        onChange={this.handleFormChange}
        onCancel={this.handleCancel}
      />
    );
  }
}

export default withRouter(Container);
