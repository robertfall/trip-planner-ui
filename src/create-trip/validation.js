export default {
  name: { presence: true },
  destination: { presence: true },
  dates_start: { presence: { message: "^Start Date can't be blank" } },
  dates_end: { presence: { message: "^End Date can't be blank" } },
};
