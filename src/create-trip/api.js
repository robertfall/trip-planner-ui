import { API_BASE_URL } from '../config';

const TRIPS_URL = `${API_BASE_URL}/trips`;

export default function createTrip(trip) {
  return fetch(TRIPS_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(trip),
  }).then(response => response.json());
}
