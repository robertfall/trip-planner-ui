import React from 'react';
import renderer from 'react-test-renderer';
import TripForm from './component';

describe('Create Trip', () => {
  test('when empty', () => {
    const values = {
      name: '',
      destination: '',
      date_start: '',
      date_end: ''
    };
    const tree = renderer.create(
      <TripForm values={values} />
    ).toJSON();

    expect(tree).toMatchSnapshot();
  })

  test('with data - no errors', () => {
    const values = {
      name: '',
      destination: '',
      date_start: '',
      date_end: ''
    };
    const tree = renderer.create(
      <TripForm values={values} />
    ).toJSON();

    expect(tree).toMatchSnapshot();
  })

  test('with data and errors', () => {
    const values = {
      name: '',
      destination: '',
      date_start: '',
      date_end: '',
      errors: {
        name: ['A name error'],
        destination: ['A destination error'],
        date_start: ['A start date error'],
        date_end: ['An en date error'],
      }
    };
    const tree = renderer.create(
      <TripForm values={values} />
    ).toJSON();

    expect(tree).toMatchSnapshot();
  })
})