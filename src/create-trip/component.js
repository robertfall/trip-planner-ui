import React from 'react';
import { FormTextField, DateRangeSelector } from '../style-guide/forms';
import { StandardContainer } from '../style-guide/containers';
import { CancelButton, SaveButton } from '../style-guide/buttons';
import { PageHeader } from '../style-guide/typography';

export default function CreateTripForm({
  onChange,
  values,
  onSubmit,
  onCancel,
}) {
  return (
    <div>
      <PageHeader>Create a New Trip</PageHeader>
      <StandardContainer>
        <form onSubmit={onSubmit}>
          <FormTextField
            name="name"
            onChange={onChange}
            values={values}
            errors={values.errors}
            key="name"
            autocomplete="false"
          />
          <FormTextField
            name="destination"
            onChange={onChange}
            values={values}
            errors={values.errors}
            key="destination"
            autocomplete="false"
          />
          { /* TODO: Find and use a richer Date Range selector */ }
          <DateRangeSelector
            name="dates"
            onChange={onChange}
            values={values}
            errors={values.errors}
            key="dates"
          />
          <SaveButton />
          &nbsp;
          <CancelButton onClick={onCancel} />
        </form>
      </StandardContainer>
    </div>
  );
}
