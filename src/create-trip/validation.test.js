import validate from 'validate.js';
import constraints from './validation';

function valid(form) {
  return validate(form, constraints);
}

describe('TripForm validation', () => {
  test('name is required', () => {
    expect(valid({ name: undefined }).name).toEqual(["Name can't be blank"]);
    expect(valid({ name: 'Present' }).name).toBeUndefined();
  });

  test('destination is required', () => {
    expect(valid({ destination: undefined }).destination).toEqual(["Destination can't be blank"]);
    expect(valid({ destination: 'Present' }).destination).toBeUndefined();
  });

  test('start date is required', () => {
    expect(valid({ dates_start: undefined }).dates_start).toEqual(["Start Date can't be blank"]);
    expect(valid({ dates_start: 'Present' }).dates_start).toBeUndefined();
  });

  test('end date is required', () => {
    expect(valid({ dates_end: undefined }).dates_end).toEqual(["End Date can't be blank"]);
    expect(valid({ dates_end: 'Present' }).dates_end).toBeUndefined();
  });
});
