import styled from 'styled-components';
import { GAINSBORO, WHITE } from './colors';
import media from './display-areas';

export const StandardContainer = styled.div`
  background-color: ${WHITE};
  margin: 1em ;
  box-sizing: border-box;
  margin: 0.5rem;
  border-bottom: 1px solid ${GAINSBORO};
  padding-bottom: 0.5rem;

  ${media.small`
    padding: 0.5rem 1rem;
    border-bottom: 0;
    box-shadow: 0px 0px 4px 0px ${GAINSBORO};
    padding: 1.5rem;
    width: 45rem;
  `}
  ${media.medium`
    padding: 2rem;
    width: 60rem;
  `}
  ${media.large`
    padding: 2.5rem;
    width: 74rem;
  `}
  ${media.extraLarge`
    padding: 3rem;
    width: 90rem;
  `}
`;
