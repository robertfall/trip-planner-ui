import styled from 'styled-components';
import { WHITE, GRAY_BLUE } from './colors';

export const NavBar = styled.ul`
  background-color: ${GRAY_BLUE};
  height: 3rem;
`;

export const NavLink = styled.li`
  color: ${WHITE};
  text-decoration: underline;
  line-height: 3rem;
  font-family: Roboto;
  font-size: 1rem;
  display: inline-block;
  padding: 0 1rem;
`;
