import styled from 'styled-components';
import media from './display-areas';
import { GUNMETAL, GAINSBORO, STEEL_BLUE, WHITE } from './colors';

export const PageHeader = styled.h1`
  font-family: Merriweather;
  color: ${GUNMETAL};
  line-height: 2;
  font-size: 1.5rem;
  margin-left: 0.5rem;
  ${media.small`
    font-size: 2rem;
    line-height: 2.75;
    margin-left: 1.5rem;
  `}
  ${media.medium`
    margin-left: 2.5rem;
  `}
  ${media.large`
    font-size: 2rem;
    margin-left: 2.5rem;
  `}
  ${media.extraLarge`
    font-size: 2.5rem;
    margin-left: 3rem;
  `}
`;

export const HeroText = styled.h1`
  color: ${GAINSBORO};
  font-family: Roboto;
  font-size: 3rem;
  box-sizing: border-box;
  display: block;
  height: 10rem;
  line-height: 10rem;
  text-align: center;
  position: relative;
`

export const SectionHeader = styled.h2`
  font-family: Roboto;
  color: ${STEEL_BLUE};
  font-size: 1rem;

  ${media.small`
    font-size: 1.5rem;
  `}
  ${media.large`
    font-size: 1.5rem;
  `}
  ${media.extraLarge`
    font-size: 2rem;
  `}
`;

export const SecondaryInformation = styled.h2`
  font-family: Roboto;
  color: ${GAINSBORO};
  font-size: 1rem;
  font-weight: 300;
  ${media.small`
    font-size: 1.5rem;
  `}
  ${media.large`
    font-size: 1.5rem;
  `}
  ${media.extraLarge`
    font-size: 2rem;
  `}
`;

