import { css } from 'styled-components';

const sizes = {
  extraLarge: 1600,
  large: 1280,
  medium: 1024,
  small: 768,
}

export default Object.keys(sizes).reduce((accumulator, label) => {
  const emSize = sizes[label] / 16
  accumulator[label] = (...args) => css`
    @media (min-width: ${emSize}em) {
      ${css(...args)}
    }
  `
  return accumulator;
}, {});
