import React from 'react';
import styled from 'styled-components';
import { STEEL_BLUE, GUNMETAL, GAINSBORO, RED } from './colors';

export const Label = styled.label`
  font-family: Roboto;
  color: ${STEEL_BLUE};
  display: block;
  font-size: 1rem;
  margin-bottom: 0.5rem;
`;

export const TextInput = styled.input`
  color: ${GUNMETAL};
  width: 100%;
  border: 1px solid ${GAINSBORO};
  font-size: 1rem;
  padding: 0.5rem;
  box-sizing: border-box; 
  margin-bottom: 0.5rem;
`;
export const FormError = styled.div`
  color: ${RED};
  font-family: Roboto;
  font-size: 1rem;
`;

function toTitleCase(str) {
  return str
    .split(/[-_]/)
    .map(s =>
      s.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      })
    )
    .join(' ');
}

// TODO: Invert control - inject value and errors
export const FormTextField = styled(
  ({ name, label, onChange, values, className, errors, ...props }) => (
    <div className={className}>
      <Label for={name}>{label || toTitleCase(name)}</Label>
      <TextInput
        name={name}
        value={values[name]}
        {...props}
        onChange={e => onChange(name, e.target.value)}
      />
      {errors &&
        errors[name] &&
        errors[name].map(error => <FormError key={error}>{error}</FormError>)}
    </div>
  )
)`
  margin-bottom: 1rem;
`;

export const DateRangeSelector = ({ name, ...props }) => (
  <div>
    <FormTextField name={`${name}_start`} label="Start" {...props} />
    <FormTextField name={`${name}_end`} label="End" {...props} />
  </div>
);
