import React from 'react';
import styled from 'styled-components';
import { STEEL_BLUE, RED, WHITE, GRAY_BLUE } from './colors';
import media from './display-areas';

export const BUTTON_BASE = `
  font-family: Roboto;
  font-size: 1rem;
  padding: 0.5rem;
  width: 7rem;
  border: none;

  ${media.small`
    width: 8em;
  `}
`;

export const CancelButton = styled((props) => (
    <button {...props}>Cancel</button>
  ))`
  ${BUTTON_BASE}
  color: ${WHITE};
  background: ${RED};
`

export const SaveButton = styled((props) => (
    <button {...props}>Save</button>
  ))`
  ${BUTTON_BASE}  
  color: ${WHITE};
  background: ${STEEL_BLUE};
`;

export const NewButton = styled((props) => (
    <button {...props}>New</button>
  ))`
  ${BUTTON_BASE}  
  color: ${WHITE};
  background: ${GRAY_BLUE};
`;