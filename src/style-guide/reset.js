import { injectGlobal } from 'styled-components';

export default function() {
  // eslint in create-react-app doesn't understand template literals
  // eslint-disable-next-line
  injectGlobal`
    body {
      margin: 0;
    }

    h1, h2 {
      margin: 0;
    }

    a {
      text-decoration: none;
      &:visited {
        color: inherit;
      }
    }

    li, ul {
      list-style: none;
      margin: 0;
      padding: 0;
    }
  `;
}
