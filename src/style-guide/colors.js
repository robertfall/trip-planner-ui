export const GUNMETAL = 'rgba(43, 45, 66, 1)';
export const GRAY_BLUE = 'rgba(141, 153, 174, 1)';
export const GAINSBORO = 'rgba(217, 222, 224, 1)';
export const RED = 'rgba(239, 35, 60, 1)';
export const STEEL_BLUE = 'rgba(63, 124, 172, 1)';
export const WHITE = `rgba(255,255,255,1)`;