import React from 'react';
import ReactDOM from 'react-dom';
import CreateTrip from './create-trip';
import ExistingTrips from './existing-trips';
import Menu from './menu';
import resetStyles from './style-guide/reset';
import { NEW_TRIP_PATH, TRIPS_PATH } from './paths';
import { NavBar, NavLink } from './style-guide/navigation';

import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

resetStyles();

class App extends React.Component {
  render() {
    return (
      <Router>
        <div>
          <NavBar>
            <NavLink><Link to="/">Home</Link></NavLink>
          </NavBar>
          <Route exact path="/" component={Menu} />
          <Route exact path={TRIPS_PATH} component={ExistingTrips} />
          <Route path={NEW_TRIP_PATH} component={CreateTrip} />
        </div>
      </Router>
    );
  }
}

ReactDOM.render(<App />, document.getElementById('root'));
