import React from 'react';
import renderer from 'react-test-renderer';
import ExistingTrips from './component';

describe('ExistingTrips', () => {
  test('when loading', () => {
    const props = {
      status: 'loading',
    };
    const tree = renderer.create(<ExistingTrips {...props} />).toJSON();

    expect(tree).toMatchSnapshot();
  });

  test('loaded with trips', () => {
    const props = {
      status: 'loaded',
      trips: [
        {
          name: "John's Wedding",
          destination: 'Johannesburg',
          dates_start: '2017-05-11',
          dates_end: '2017-05-14',
          id: 1,
        },
        {
          name: 'Family Vacation',
          destination: 'Hawaii',
          dates_start: '2018-05-11',
          dates_end: '2019-05-14',
          id: 2,
        },
      ],
    };
    const tree = renderer.create(<ExistingTrips {...props} />).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
