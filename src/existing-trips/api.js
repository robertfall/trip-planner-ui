import { API_BASE_URL } from '../config';

const TRIPS_URL = `${API_BASE_URL}/trips`;

export function getTrips() {
  return fetch(TRIPS_URL, { method: 'GET' })
    .then(response => response.json());
}
