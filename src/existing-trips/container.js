import React from 'react';
import { withRouter } from 'react-router-dom';
import Component from './component';
import { getTrips } from './api';

class Container extends React.Component {
  constructor() {
    super();

    this.state = {
      status: 'not-started',
    };
  }

  componentDidMount() {
    this.setState(() => ({ status: 'loading' }));
    getTrips()
      .then(trips => {
        this.setState(state => ({ ...state, trips, status: 'loaded' }));
      })
      // TODO: Add an error state component
      .catch(err => console.error(err));
  }
  render() {
    const { status, trips } = this.state;
    return (
      <Component
        trips={trips}
        status={status}
      />
    );
  }
}

export default withRouter(Container);
