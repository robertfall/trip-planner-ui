import React from 'react';
import styled from 'styled-components';
import {
  PageHeader,
  SectionHeader,
  SecondaryInformation,
} from '../style-guide/typography';
import { StandardContainer } from '../style-guide/containers';
import { Hourglass } from '../style-guide/loading';

const Trip = styled(({ name, destination, dates_start, dates_end }) => (
  <StandardContainer>
    <SectionHeader>{name}</SectionHeader>
    <SecondaryInformation>
      to {destination}
      <br />
      from {dates_start} to {dates_end}
    </SecondaryInformation>
  </StandardContainer>
))``;

export default ({ status = 'loading', trips }) => (
  <div>
    <PageHeader>Existing Trips</PageHeader>
    {status === 'loaded' && trips.map(t => <Trip key={t.id} {...t} />)}
    {status === 'loading' && <Hourglass />}
  </div>
);
