import React from 'react';
import { Link } from 'react-router-dom';
import { StandardContainer } from '../style-guide/containers';
import { HeroText } from '../style-guide/typography';
import { NEW_TRIP_PATH, TRIPS_PATH } from '../paths';

export default () => (
  <div>
    <Link to={NEW_TRIP_PATH}>
      <StandardContainer>
        <HeroText>
          New Trip
        </HeroText>
      </StandardContainer>
    </Link>

    <Link to={TRIPS_PATH}>
      <StandardContainer>
        <HeroText>
          Existing Trips
        </HeroText>
      </StandardContainer>
    </Link>
  </div>
);
